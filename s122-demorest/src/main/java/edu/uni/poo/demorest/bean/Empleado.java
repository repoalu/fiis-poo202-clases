package edu.uni.poo.demorest.bean;

public class Empleado {
    private String codigo;
    private String nombre;
    private float salario;
    
    public Empleado(String codigo, String nombre, float salario){
        this.codigo = codigo;
        this.nombre = nombre;
        this.salario = salario;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getSalario() {
        return salario;
    }

    public void setSalario(float salario) {
        this.salario = salario;
    }
    
}
