package edu.uni.poo.demorest.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import edu.uni.poo.demorest.bean.Empleado;

@RestController
public class EmpleadoController {
    @Autowired
    JdbcTemplate template;

    @RequestMapping("/empleados")
    public List<Empleado> obtenerEmpleados(){
        List<Empleado> lista = new ArrayList<>();
        String sql = "SELECT * FROM EMPLEADO";
        List<Map<String, Object>> registros = template.queryForList(sql);
        for(Map<String, Object> mapaEmp: registros){
            String codigo = (String) mapaEmp.get("CODIGO");
            String nombre = (String) mapaEmp.get("NOMBRE");
            float salario = ((BigDecimal) mapaEmp.get("SALARIO")).floatValue();
            lista.add(new Empleado(codigo, nombre, salario));
        }
        return lista;
    }


}
