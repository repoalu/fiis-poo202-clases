package edu.uni.poo.demorest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class ProyectoController {
    @Autowired
    JdbcTemplate template;

    @PostMapping("/proyectos")
    public String registrarProyecto(@RequestParam Integer cod, @RequestParam String nom) {
        System.out.println("Codigo: " + cod);
        System.out.println("Nombre: " + nom);
        String sql = "INSERT INTO PROYECTO VALUES(?, ?)";
        template.update(sql, cod, nom);
        return "redirect:/confirmacion.html";
    }
}
