package edu.uni.poo.demorest.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import edu.uni.poo.demorest.bean.Departamento;

@RestController
public class DepartamentoController {
    @Autowired
    JdbcTemplate template;

    @GetMapping("/departamentos")
    public List<Departamento> obtenerDepartamentos(){
        List<Departamento> res = new ArrayList<>();
        String sql = "SELECT * FROM DEPARTAMENTO";
        List<Map<String, Object>> deptos = template.queryForList(sql);
        //Por cada tupla de la tabla
        for(Map<String, Object> mapaDpto: deptos){
            int codigo = (Integer) mapaDpto.get("CODIGO");
            String nombre = (String) mapaDpto.get("NOMBRE");
            String direccion = (String) mapaDpto.get("DIRECCION");
            Departamento d1 = new Departamento(codigo, nombre, direccion);
            res.add(d1);
        }
        return res;
    }

    @GetMapping("/departamentos/{codigo}")
    public Departamento obtenerDepartamento(@PathVariable int codigo){
        Departamento dep = null;
        String sql = "SELECT * FROM DEPARTAMENTO WHERE CODIGO = ?";
        SqlRowSet rs = template.queryForRowSet(sql, codigo);
        if(rs.next()){
            String nombre = rs.getString("NOMBRE");
            String direccion = rs.getString("DIRECCION");
            dep = new Departamento(codigo, nombre, direccion);
        }
        return dep;
    }


    @PostMapping("/departamentos")
    //{"codigo": 120, "nombre": "Depto1", "descripcion": "SJL"}
    public Departamento registrarDepartamento(@RequestBody Departamento depto) {
        String sql = "INSERT INTO DEPARTAMENTO VALUES(?, ?, ?)";
        template.update(sql, depto.getCodigo(), depto.getNombre(), depto.getDireccion());
        return depto;
    }

}
