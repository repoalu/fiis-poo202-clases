package edu.uni.poo.demorest.controller;

import java.net.InetAddress;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SaludoController {
    @RequestMapping("/saludo")
    public String saludar(){
        return "<h1>Hola FIIS</h1>";         
    }
    //localhost:8080/info
    @RequestMapping("/info")
    public String getInfo() throws Exception{
        String res = "";
        InetAddress address = InetAddress.getLocalHost();
        String host = address.getHostName();
        String ip = address.getHostAddress();

        res += "Fecha y Hora: " + new java.util.Date() + " || ";
        res += "Direccion IP:  " + ip + " || ";
        res += "Nombre de Host: " + host + "";
        return res;
    }
}
