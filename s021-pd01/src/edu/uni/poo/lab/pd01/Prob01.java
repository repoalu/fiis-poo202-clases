package edu.uni.poo.lab.pd01;

import java.util.Scanner;

public class Prob01 {
    public static float obtenerPromedio(int a, int b){
        float promedio = (a + b) / 2.0f;
        return promedio;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese valor de a: ");
        int valor1 = sc.nextInt();

        System.out.print("Ingrese el valor de b: ");
        int valor2 = sc.nextInt();

        float prom = obtenerPromedio(valor1, valor2);
        System.out.println("El promedio es: " + prom);
        sc.close();
    }
}

