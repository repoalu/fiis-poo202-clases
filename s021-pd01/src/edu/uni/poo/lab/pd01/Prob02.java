package edu.uni.poo.lab.pd01;

import java.util.Scanner;

public class Prob02 {
    public static int calcularSuma(int n){
        int resultado = 0;
        for(int i = 1; i <= n; i++){            
            resultado = resultado + i;  //resultado += i;
        }
        return resultado;        
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese valor: ");
        int n = sc.nextInt();
        int suma = calcularSuma(n);
        System.out.println("Suma: " + suma);
        sc.close();
    }
}
