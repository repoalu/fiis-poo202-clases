package edu.uni.poo.lab.pd01;

import java.util.Arrays;
import java.util.Scanner;

public class Prob03 {
    public static int variable = 40;
    public static int[] leerValores(int n){
        int[] resultado = new int[n];
        Scanner sc = new Scanner(System.in);
        for(int i = 0; i < n; i++){
            System.out.println("Ingrese nota alumno " + (i + 1) + ": ");
            int nota = sc.nextInt();
            resultado[i] = nota;
        }
        sc.close();
        return resultado;
    }
    public static void main(String[] args) {
        int n = 4;
        int[] resultado = leerValores(n);
        System.out.println(Arrays.toString(resultado));
    }

}
