package edu.uni.poo.lab.pd01;

public class Prob07 {
    public static void main(String[] args) {
        Alumno a1 = new Alumno("Juan", "Perez");
        a1.agregarNota(20);
        a1.agregarNota(17);
        a1.agregarNota(15);
        a1.mostrarInformacion();
    }    
}

class Alumno{
    private String nombre;
    private String apellido;
    private int[] notas;
    private int cantidadNotas;
    private static final int MAX_NOTAS = 10;
    
    //Constructor
    public Alumno(String nombre, String apellido){
       this.nombre = nombre;
       this.apellido = apellido;
       this.notas = new int[MAX_NOTAS];
       this.cantidadNotas = 0;
    }

    public void agregarNota(int nota){
        notas[cantidadNotas] = nota;
        cantidadNotas++;
    }

    public float obtenerPromedio(){
        int suma = 0;
        float promedio = 0;
        for(int i = 0; i < this.cantidadNotas; i++){
            suma = suma + notas[i];
        }
        promedio = 1.0f * suma / this.cantidadNotas;
        return promedio;
    }

    public void mostrarInformacion(){
        System.out.println("Nombre: " + this.nombre);
        System.out.println("Apellido: " + this.apellido);
        System.out.println("Lista de Notas: ");
        for(int i = 0; i < this.cantidadNotas; i++){
            System.out.println(this.notas[i]);
        }
        System.out.println("Promedio: " + this.obtenerPromedio());
    }
}