package edu.uni.poo.s04.clase.test;

import java.util.ArrayList;

import edu.uni.poo.s04.clase.Banco;
import edu.uni.poo.s04.clase.Empresa;
import edu.uni.poo.s04.clase.MotorImpuestos;
import edu.uni.poo.s04.clase.Universidad;

public class Pruebas {
    public static void main(String[] args) {
        Empresa e = new Empresa("Empresa 1");
        e.setDireccion("Av. Cordialidad 100");
        e.setUtilidadUltimoAnio(30000);
        
        Universidad u = new Universidad("Universidad del Valle");
        u.setDireccion("Av. Juventud s/n");
        u.setUtilidadUltimoAnio(40000);
        u.setCantidadAlumnos(1500);

        Banco b = new Banco("BBVA Valle");
        b.setDireccion("Jr. Concordia 3030");
        b.setUtilidadUltimoAnio(250000);
        b.setCantidadAgencias(300);

        ArrayList<Empresa> listaEmpresas = new ArrayList<>();
        listaEmpresas.add(e);
        listaEmpresas.add(u);
        listaEmpresas.add(b);
        MotorImpuestos motor = new MotorImpuestos(listaEmpresas);
        motor.mostrarReporte();
        float impuestoTotal = motor.obtenerImpuesto();
        System.out.println("Impuestos totales: " + impuestoTotal);
    }
}
