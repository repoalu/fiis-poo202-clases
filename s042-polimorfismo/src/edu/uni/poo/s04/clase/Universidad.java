package edu.uni.poo.s04.clase;

public class Universidad extends Empresa{
    private int cantidadAlumnos;
    public Universidad(String razonSocial){
        super(razonSocial);
    }

    public int getCantidadAlumnos() {
        return cantidadAlumnos;
    }

    public void setCantidadAlumnos(int cantidadAlumnos) {
        this.cantidadAlumnos = cantidadAlumnos;
    }
    
}
