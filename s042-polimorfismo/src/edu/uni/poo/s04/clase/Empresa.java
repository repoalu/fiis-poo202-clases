package edu.uni.poo.s04.clase;

public class Empresa {
    private String razonSocial;
    private String nombreComercial;
    private String direccion;
    private String telefono;
    private float utilidadUltimoAnio;

    public Empresa(String razonSocial){
        this.razonSocial = razonSocial;
    }

    public void mostrarDatos(){
        System.out.println("Razon Social: " + this.razonSocial);
        System.out.println("Direccion: " + this.direccion);

    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public float getUtilidadUltimoAnio() {
        return utilidadUltimoAnio;
    }

    public void setUtilidadUltimoAnio(float facturacionUltimoAnio) {
        this.utilidadUltimoAnio = facturacionUltimoAnio;
    }


}
