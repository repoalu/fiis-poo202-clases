package edu.uni.poo.s04.clase;

import java.util.ArrayList;

//Calcular el impuesto en funcion a la utilidad del ultimo anio
public class MotorImpuestos {
    private ArrayList<Empresa> empresas;
    private static final float RATIO_IMPUESTOS = 0.3f;

    public MotorImpuestos(ArrayList<Empresa> empresas){
        this.empresas = empresas;
    }

    //Muestra el reporte de las empresas analizadas
    public void mostrarReporte(){
        for(int i = 0; i < this.empresas.size(); i++){
            Empresa e = this.empresas.get(i);
            e.mostrarDatos();
            System.out.println("Utilidades ultimo anio: " + e.getUtilidadUltimoAnio());
            System.out.println("***************************");
        }
    }

   //Retorna la cantidad total de impuesto por pagar
   public float obtenerImpuesto(){
        float total = 0;
        for(Empresa e: empresas){
            total = total + e.getUtilidadUltimoAnio();
        }
        return total * RATIO_IMPUESTOS;
   }
}
