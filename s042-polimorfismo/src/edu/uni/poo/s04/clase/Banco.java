package edu.uni.poo.s04.clase;

public class Banco extends Empresa{
    private int cantidadAgencias;
    public Banco(String razonSocial){
        super(razonSocial);
    }

    public int getCantidadAgencias() {
        return cantidadAgencias;
    }

    public void setCantidadAgencias(int cantidadAgencias) {
        this.cantidadAgencias = cantidadAgencias;
    }

    public void mostrarDatos(){
        super.mostrarDatos();
        System.out.println("Cantidad agencias: " + this.cantidadAgencias);
    }
    
}
