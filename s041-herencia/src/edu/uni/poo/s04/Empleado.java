package edu.uni.poo.s04;

public class Empleado extends Persona{
    private float salario;
    public Empleado(String nombre, int edad, float salario){
        super(nombre, edad);
        this.salario = salario;
    }

    public void mostrarDatos(){
        super.mostrarDatos();
        System.out.println("Salario: " + this.salario);
    }

}
