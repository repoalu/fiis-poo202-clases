package edu.uni.poo.s04.test;

import java.util.ArrayList;

import edu.uni.poo.s04.Empleado;
import edu.uni.poo.s04.Persona;

public class PruebaPersona {

    public static void mostrarDatos(ArrayList<Persona> lista){
        for(Persona p: lista){
            p.mostrarDatos();
            System.out.println("***********");
        }
    }
    public static void main(String[] args) {
        Persona p = new Persona("Juan Perez", 20);        
        Empleado e = new Empleado("Cesar Alvarez", 22, 6000);

        ArrayList<Persona> lista = new ArrayList<>();
        lista.add(p);
        lista.add(e);

        mostrarDatos(lista);

    }
}
