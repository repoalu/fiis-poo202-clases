package edu.uni.poo.colecciones;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PruebaColecciones {
    public static void main(String[] args) {
        Collection<String> facultades = new ArrayList<>();
        facultades.add("FIIS");
        facultades.add("FIEE");
        facultades.add("FC");
        System.out.println(facultades.size());
        System.out.println(facultades);
        facultades.clear();
        System.out.println(facultades);
        
    }
}
