package edu.uni.poo.colecciones;

import java.util.Iterator;
import java.util.LinkedList;

import edu.uni.poo.colecciones.bean.Alumno;

public class PruebaLinkedList {
    public static void main(String[] args) {
        LinkedList<Alumno> lista = new LinkedList<>();
        Alumno a1 = new Alumno("340F", "J Perez");
        lista.addFirst(a1);
        lista.addFirst(new Alumno("450V", "C Alvarez"));
        lista.addLast(new Alumno("344Z", "W Jara"));
        lista.addFirst(new Alumno("757Y", "J Vargas"));
                
        //Recorrido
        Iterator<Alumno> it = lista.iterator();
        while(it.hasNext()){
            System.out.println(it.next());
        }
        
        //Busqueda
        Alumno aluBuscar = new Alumno("340F", "<SIN DATOS>");
        System.out.println(lista);
        lista.remove(aluBuscar);
        System.out.println(lista);
    }
}
