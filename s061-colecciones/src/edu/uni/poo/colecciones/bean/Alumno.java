package edu.uni.poo.colecciones.bean;

public class Alumno {
    private String codigo;
    private String nombre;
    public Alumno(String codigo, String nombre){
        this.codigo = codigo;
        this.nombre = nombre;
    }
    
    public String toString(){
        return this.codigo + "-" + this.nombre;
    }

    public boolean equals(Object objeto){
        boolean res = false;
        if(objeto instanceof Alumno){
            Alumno a1 = (Alumno) objeto;
            if(a1.getCodigo().equals(this.codigo)){
                res = true;
            }
        }
        return res;
    }

    public String getCodigo() {
        return codigo;
    }

}
