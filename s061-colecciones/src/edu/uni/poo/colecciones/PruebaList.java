package edu.uni.poo.colecciones;

import java.util.ArrayList;
import java.util.List;

public class PruebaList {
    public static void main(String[] args) {
        List<Integer> codigos = new ArrayList<Integer>();
        codigos.add(120324);
        codigos.add(234443);
        codigos.add(123455);
        codigos.add(848590);
        System.out.println(codigos);
        codigos.add(2, 747850);
        System.out.println(codigos);
        int idx = codigos.indexOf(848590);
        System.out.println(idx);

    }
}
