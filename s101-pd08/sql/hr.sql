CREATE TABLE REGIONS(
    REGION_ID INTEGER PRIMARY KEY,
    REGION_NAME VARCHAR(50)    
);

INSERT INTO REGIONS VALUES(1, 'Europa');
INSERT INTO REGIONS VALUES(2, 'America Latina');
INSERT INTO REGIONS VALUES(3, 'Estados Unidos');

CREATE TABLE COUNTRIES(
    COUNTRY_ID INTEGER PRIMARY KEY,
    COUNTRY_NAME VARCHAR(50),
    REGION_ID INTEGER REFERENCES REGIONS
);

INSERT INTO COUNTRIES VALUES(1, 'Peru', 2);
INSERT INTO COUNTRIES VALUES(2, 'Argentina', 2);
INSERT INTO COUNTRIES VALUES(3, 'Italia', 1);
INSERT INTO COUNTRIES VALUES(4, 'Alemania', 1);
INSERT INTO COUNTRIES VALUES(11, 'Dinamarca', 1);

SELECT C.COUNTRY_ID, C.COUNTRY_NAME, R.REGION_NAME
FROM COUNTRIES C, REGIONS R
WHERE R.REGION_ID = C.REGION_ID;

SELECT COUNTRY_ID, COUNTRY_NAME FROM countries;
