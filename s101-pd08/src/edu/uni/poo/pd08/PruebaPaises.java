package edu.uni.poo.pd08;
import java.sql.*;

public class PruebaPaises {
    public static void registrarPais(int codPais, String nombrePais, int codRegion) throws Exception{
        //Registrar la clase Driver
        Class.forName("org.postgresql.Driver");
        //Obtener el objeto Conexion
        String url = "jdbc:postgresql://localhost:5432/postgres";
        String usuario = "postgres";
        Connection conn = DriverManager.getConnection(url, usuario, "");
        //Crear objeto Statement
        String sql = "INSERT INTO COUNTRIES VALUES(?, ?, ?)";
        PreparedStatement pst = conn.prepareStatement(sql);
        //Configurar parametros
        pst.setInt(1, codPais);
        pst.setString(2, nombrePais);
        pst.setInt(3, codRegion);
        //Ejecutar Sentencia
        pst.executeUpdate();
        System.out.println("Pais registrado correctamente");
        //Liberar recursos
        pst.close();
        conn.close();
    }

    public static void mostrarPaises() throws Exception{
        //Registrar la clase Driver
        Class.forName("org.postgresql.Driver");
        //Obtener el objeto Conexion
        String url = "jdbc:postgresql://localhost:5432/postgres";
        String usuario = "postgres";
        Connection conn = DriverManager.getConnection(url, usuario, "");
        //Crear objeto Statement
        String sql = "SELECT COUNTRY_ID, COUNTRY_NAME FROM COUNTRIES";
        PreparedStatement pst = conn.prepareStatement(sql);
        //Ejecutar Sentencia
        ResultSet rs = pst.executeQuery();
        while(rs.next()){
            int codigo = rs.getInt(1);
            String nombre = rs.getString(2);
            System.out.println("Codigo: " + codigo);
            System.out.println("Nombre: " + nombre);
            System.out.println("*****************************");
        }
        //Liberar recursos
        rs.close();
        pst.close();
        conn.close();
    }

    public static void main(String[] args) throws Exception{
        mostrarPaises();
    }
}
