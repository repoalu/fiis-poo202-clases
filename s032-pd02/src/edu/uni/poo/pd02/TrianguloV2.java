package edu.uni.poo.pd02;

public class TrianguloV2 {
    private int a;
    private int b;
    private int c;
    
    public TrianguloV2(int a, int b, int c){
        this.a = a;
        this.b = b;
        this.c = c;        
    }
    public int obtenerPerimetro(){
        int perimetro = this.a + this.b + this.c;
        return perimetro;        
    }
}
