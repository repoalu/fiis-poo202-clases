package edu.uni.poo.pd02;

public class TrianguloV3 {
    private int a;
    private int b;
    private int c;
    private int perimetro;

    public TrianguloV3(int a, int b, int c){
        this.a = a;
        this.b = b;
        this.c = c;
        this.perimetro = a + b + c;
    }
    public int getPerimetro(){
        return this.perimetro;
    }
}
