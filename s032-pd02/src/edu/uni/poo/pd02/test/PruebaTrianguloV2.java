package edu.uni.poo.pd02.test;

import edu.uni.poo.pd02.TrianguloV2;

public class PruebaTrianguloV2 {
    public static void main(String[] args) {
        TrianguloV2 t1 = new TrianguloV2(3, 4, 5);
        int per1 = t1.obtenerPerimetro();
        
        System.out.println("Perimetro 1: " + per1);

        TrianguloV2 t2 = new TrianguloV2(5, 5, 5);
        int per2 = t2.obtenerPerimetro();
        System.out.println("Perimetro 2: " + per2);
    }
}
