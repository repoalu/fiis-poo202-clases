package edu.uni.poo.pd02.test;

import edu.uni.poo.pd02.TrianguloV3;

public class PruebaTrianguloV3 {
    public static void main(String[] args) {
        TrianguloV3 t1 = new TrianguloV3(3, 4, 5);
        int per1 = t1.getPerimetro();
        System.out.println("Perimetro 1: " + per1);

        TrianguloV3 t2 = new TrianguloV3(5, 5, 5);
        int per2 = t2.getPerimetro();
        System.out.println("Perimetro 2: " + per2);
    }
}
