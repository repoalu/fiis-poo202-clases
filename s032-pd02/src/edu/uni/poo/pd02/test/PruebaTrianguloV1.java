package edu.uni.poo.pd02.test;

import edu.uni.poo.pd02.TrianguloV1;

public class PruebaTrianguloV1 {
    public static void main(String[] args) {
        int a = 3;
        int b = 5;
        int c = 4;
        int perimetro = TrianguloV1.obtenerPerimetro(a, b, c);
        System.out.println("Perimetro: " + perimetro);
    }
}
