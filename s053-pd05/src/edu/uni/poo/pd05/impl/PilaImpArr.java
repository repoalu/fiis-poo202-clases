package edu.uni.poo.pd05.impl;

import edu.uni.poo.pd05.Pila;

public class PilaImpArr implements Pila {
    private static int TAM_MAX = 50;
    private int[] valores;
    private int cima;

    public PilaImpArr(){
        valores = new int[TAM_MAX];
        cima = -1;
    }

    public void push(Integer item){
        cima++;
        valores[cima] = item;
    }

    public Integer pop(){
        return 0;
    }

    public boolean estaVacia(){
        return false;
    }

    public void mostrarElementos(){

    }
}
