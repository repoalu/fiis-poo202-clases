package edu.uni.poo.pd05.impl.test;

import edu.uni.poo.pd05.Pila;
import edu.uni.poo.pd05.impl.PilaImpVar;

public class PruebaPilaVar {
    public static void main(String[] args) {
        Pila p = new PilaImpVar();
        p.push(12);
        p.push(18);
        p.push(35);
        p.mostrarElementos();
        int elem = p.pop();
        System.out.println("Elemento extraido: " + elem);
        p.mostrarElementos();
    }

}
