package edu.uni.poo.pd05.impl;

import edu.uni.poo.pd05.Pila;

public class PilaImpVar implements Pila {
    private Integer elemento1;
    private Integer elemento2;
    private Integer elemento3;

    public PilaImpVar(){
        this.elemento1 = null;
        this.elemento2 = null;
        this.elemento3 = null;
    }

    public void push(Integer valor){
        if(this.elemento1 == null){
            this.elemento1 = valor;
        }else if(this.elemento2 == null){
            this.elemento2 = valor;
        }else if(this.elemento3 == null){
            this.elemento3 = valor;
        }
    }

    public Integer pop(){
        Integer respuesta = null;
        if(this.elemento3 != null){
            respuesta = this.elemento3;
            this.elemento3 = null;
        }else if(this.elemento2 != null){
            respuesta = this.elemento2;
            this.elemento2 = null;
        }else if(this.elemento1 != null){
            respuesta = this.elemento1;
            this.elemento1 = null;
        }
        return respuesta;
    }
    public boolean estaVacia(){
        boolean respuesta;
        if(this.elemento1 == null){
            respuesta = true;
        }else{
            respuesta = false;
        }
        return respuesta;
    }
    public void mostrarElementos(){
        if(this.elemento3 != null){
            System.out.println(this.elemento3);
        }
        if(this.elemento2 != null){
            System.out.println(this.elemento2);
        }
        if(this.elemento1 != null){
            System.out.println(this.elemento1);
        }        
    }
}
