package edu.uni.poo.pd05;

public interface Pila {
    public void push(Integer valor);
    public Integer pop();
    public boolean estaVacia();
    public void mostrarElementos();
}
