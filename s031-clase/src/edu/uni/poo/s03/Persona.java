package edu.uni.poo.s03;

public class Persona {
    //Atributos
    private String nombre;
    private int edad;
    private static final int EDAD_MAX = 180;

    //Constructores
    public Persona(String nombre){
        this.nombre = nombre;
    }
    
    public Persona(String nombre, int edad){
        this.nombre = nombre;
        this.setEdad(edad);   
    }

    //Metodos
    public void correr(){
        if(this.edad < 70){
            System.out.println(nombre + " esta corriendo");
        }else{
            System.out.println(nombre + " esta trotando");
        }
    }

    //Getters y setters
    public void setEdad(int edad){
        if(edad >= 0 && edad < EDAD_MAX){
            this.edad = edad;
        }else{
            System.out.println("Valor de edad no es valido");
        }
    }

    public int getEdad(){
        return this.edad;
    }
    
    //Metodo toString
    public String toString(){
        return nombre + " - " + edad;
    }

    public static boolean mayorQue(Persona p1, Persona p2){
        return (p1.getEdad() > p2.getEdad());    
    }
    
}
