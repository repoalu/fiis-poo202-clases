package edu.uni.poo.s03.test;
import edu.uni.poo.s03.Persona;

public class Prueba {
    public static void main(String[] args) {        
        Persona p1 = new Persona("Juan Perez");
        Persona p2 = new Persona("Maria Alvarez", 560);
        p1.correr();
        p2.correr();
        System.out.println(p1);
        System.out.println(p2);
        p2.setEdad(-123);
        System.out.println(p2);
        
    }
}
