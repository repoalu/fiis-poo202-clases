package edu.uni.poo.s05;

public abstract class Empresa {
    private String razonSocial;
    private String direccion;

    public Empresa(String razonSocial, String direccion){
        this.razonSocial = razonSocial;
        this.direccion = direccion;
    }

    public void mostrarDatos(){
        System.out.println(razonSocial);
        System.out.println(direccion);
    }

    public abstract float calcularImpuesto();
}
