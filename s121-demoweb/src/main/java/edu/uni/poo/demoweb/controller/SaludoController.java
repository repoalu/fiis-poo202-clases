package edu.uni.poo.demoweb.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SaludoController {
    @RequestMapping("/saludo")
    public ResponseEntity<String> saludar(){
        String saludo = "Hola mundo";
        return new ResponseEntity<String>(saludo, HttpStatus.OK);
    }
}
