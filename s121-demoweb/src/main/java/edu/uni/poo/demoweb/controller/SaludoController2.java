package edu.uni.poo.demoweb.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SaludoController2 {
    @RequestMapping("/saludo2")
    public String saludar2(){
        return "Hola mundo2";
    }
}
