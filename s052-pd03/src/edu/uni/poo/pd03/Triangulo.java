package edu.uni.poo.pd03;

import java.util.ArrayList;

public class Triangulo extends Poligono {
    public Triangulo(double a, double b, double c){
        super(3);
        this.agregarLado(a);
        this.agregarLado(b);
        this.agregarLado(c);
    }

    public void mostrarTipoTriangulo(){
        ArrayList<Double> lados = this.getLados();
        double a = lados.get(0);
        double b = lados.get(1);
        double c = lados.get(2);
        if(a == b && b == c){
            System.out.println("Triangulo equilatero");
        }else if(a == b || b == c || a == c){
            System.out.println("Triangulo isosceles");
        }else{
            System.out.println("Triangulo escaleno");
        }
    }

    public void mostrarReporte(){
        System.out.println("El poligono es un triangulo");
        super.mostrarReporte();
        this.mostrarTipoTriangulo();
    }
}
