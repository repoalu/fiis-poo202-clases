package edu.uni.poo.pd03;

public class Cuadrado extends Cuadrilatero{
    public Cuadrado(double lado){
        super(lado, lado, lado, lado);
    }

    public Double obtenerArea(){
        double lado = this.getLados().get(0);
        return Math.pow(lado, 2);
    }

    public void mostrarReporte(){
        System.out.println("El poligono es un cuadrado");
        super.mostrarReporte();
        System.out.println("El area es: " + this.obtenerArea());
    }

}
