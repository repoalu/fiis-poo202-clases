package edu.uni.poo.pd03;

import java.util.ArrayList;

public class VisualizadorPoligonos {
    private ArrayList<Poligono> listaPoligonos;

    public VisualizadorPoligonos(){
        this.listaPoligonos = new ArrayList<>();
    }

    public void agregarPoligono(Poligono p){
        this.listaPoligonos.add(p);
    }

    public void mostrarReporte(){
        for(Poligono p: listaPoligonos){
            p.mostrarReporte();
            System.out.println("*****************************");
        }
    }
}
