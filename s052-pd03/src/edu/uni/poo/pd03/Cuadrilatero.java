package edu.uni.poo.pd03;

public class Cuadrilatero extends Poligono{
    public Cuadrilatero(double a, double b, double c, double d){
        super(4);
        this.agregarLado(a);
        this.agregarLado(b);
        this.agregarLado(c);
        this.agregarLado(d);
    }
}
