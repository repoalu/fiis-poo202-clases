package edu.uni.poo.pd03;
import java.util.ArrayList;

public class Poligono {
    private ArrayList<Double> lados;
    private int cantidadLados;
    public Poligono(int cantidadLados){
        this.cantidadLados = cantidadLados;
        this.lados = new ArrayList<>();
    }

    public void agregarLado(Double longitud){
        if(this.lados.size() < this.cantidadLados){
            this.lados.add(longitud);
        }else{
            System.out.println("No es posible agregar lado " + longitud);
        }
    }

    public void mostrarReporte(){
        if(this.lados.size() == this.cantidadLados){
            System.out.println("Cant. Lados: " + this.cantidadLados);
            System.out.println("Longitudes: ");
            for(int i = 0; i < this.lados.size(); i++){
                System.out.println(this.lados.get(i)); //lados[i]
            }
        }else{
            System.out.println("El poligono no tiene los lados completos");
        }
    }

    public Double obtenerPerimetro(){
        double suma = 0;
        for(int i = 0; i < this.lados.size(); i++){
            suma += this.lados.get(i);
        }
        return suma;
    }

    public ArrayList<Double> getLados() {
        return lados;
    }
    
}
