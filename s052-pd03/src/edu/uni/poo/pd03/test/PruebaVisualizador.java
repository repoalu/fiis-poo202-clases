package edu.uni.poo.pd03.test;

import edu.uni.poo.pd03.Cuadrado;
import edu.uni.poo.pd03.Cuadrilatero;
import edu.uni.poo.pd03.Poligono;
import edu.uni.poo.pd03.Triangulo;
import edu.uni.poo.pd03.VisualizadorPoligonos;

public class PruebaVisualizador {
    public static void main(String[] args) {
        Poligono p1 = new Poligono(5);
        p1.agregarLado(12.0);
        p1.agregarLado(16.0);
        p1.agregarLado(23.5);
        p1.agregarLado(18.6);
        p1.agregarLado(23.0);

        Triangulo t1 = new Triangulo(3, 4, 5);
        Cuadrilatero c1 = new Cuadrilatero(4, 8, 4, 8);
        Cuadrado cua1 = new Cuadrado(7);

        VisualizadorPoligonos v1 = new VisualizadorPoligonos();
        v1.agregarPoligono(p1);
        v1.agregarPoligono(t1);
        v1.agregarPoligono(c1);
        v1.agregarPoligono(cua1);
        v1.mostrarReporte();
    }
}
