package edu.uni.poo.pd03.test;

import edu.uni.poo.pd03.Triangulo;

public class PruebaTriangulo {
    public static void main(String[] args) {
        Triangulo t = new Triangulo(6, 6, 6);
        t.mostrarReporte();
        System.out.println("Perimetro: " + t.obtenerPerimetro());
    }
}
