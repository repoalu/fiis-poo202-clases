package edu.uni.poo.pd03.test;

import edu.uni.poo.pd03.Poligono;

public class PruebaPoligono {
    public static void main(String[] args) {
        Poligono p1 = new Poligono(3);
        p1.agregarLado(3.0);
        p1.agregarLado(4.0);
        p1.agregarLado(5.0);
        p1.mostrarReporte();
        System.out.println("Perimetro: " + p1.obtenerPerimetro());
    }
}
